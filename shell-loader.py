import json
import os, stat
import sys
from subprocess import Popen, PIPE

os.chmod('/data/user_script', stat.S_IRWXU)

with open('/data/args.json') as f:    
    args = json.load(f)

env=dict(os.environ, **args)

command = ['/data/user_script']
for key, value in args.iteritems():
    command.extend(['-%s' % key, str(value)])

p = Popen(command, env=env, shell=True)

exit_code = p.wait()
sys.exit(exit_code)